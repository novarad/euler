namespace Euler {
	public class Problem2 : IProblem2 {
		public uint SumOfEvenFibonacciNumbers(uint maxValue) {
			uint numberBeforeLast = 1;
			uint lastNumber = 1;
			uint sum = 0;
			
			do {
				uint current = numberBeforeLast + lastNumber;
				if (current % 2 == 0) {
					sum += current;
				}
				numberBeforeLast = lastNumber;
				lastNumber = current;
			} while(lastNumber < maxValue);

			return sum; //shut up vs
		}
	}
}
