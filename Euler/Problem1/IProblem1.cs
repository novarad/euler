﻿namespace Euler {
    public interface IProblem1 {
        int SumOfMultiplesOfN(int input);
    }
}