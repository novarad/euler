﻿namespace Euler {
    public class Problem1 : IProblem1 {
        public int SumOfMultiplesOfN(int input) {
            var result = 0;

            for (var i = 3; i < input; i++) {
                if (i % 3 == 0 || i % 5 == 0) {
                    result += i;
                }
            }

            return result;
        }
    }
}
