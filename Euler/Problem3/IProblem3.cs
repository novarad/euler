namespace Euler {
	public interface IProblem3 {
		ulong LargestPrimeFactorOfN(ulong n);
		bool IsPrime(ulong number);
	}
}