using System;
using System.Collections.Generic;

namespace Euler {
	public class Problem3 : IProblem3 {
		private Dictionary<ulong, bool> IsPrimeCache = new Dictionary<ulong, bool>();

		public ulong LargestPrimeFactorOfN(ulong n) {
			while (n % 2 == 0) {
				n = n / 2;
			}
			ulong largestPrimeFactor = 0;
			ulong stop = Math.Max(3,(ulong)Math.Sqrt(n) +1);
			for(ulong i = 3; i <= stop; i += 2) {
				if (n % i == 0 && IsPrime(i)) {
					largestPrimeFactor = i;
				}
			}
			return largestPrimeFactor;
		}

		public bool IsPrime(ulong number) {
			if (IsPrimeCache.ContainsKey(number)) {
				return IsPrimeCache[number];
			}

			if (number % 2 == 0) {
				IsPrimeCache.Add(number, false);
				return false;
			}

			ulong stop = (ulong)Math.Sqrt(number) + 1;
			for (ulong i = 3; i < stop; i += 2) {
				if (number % i == 0) {
					IsPrimeCache.Add(number, false);
					return false;
				}
			}

			IsPrimeCache.Add(number, true);
			return true;
		}
	}
}