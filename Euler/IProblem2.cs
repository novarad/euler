namespace Euler {
	public interface IProblem2 {
		uint SumOfEvenFibonacciNumbers(uint maxValue);
	}
}