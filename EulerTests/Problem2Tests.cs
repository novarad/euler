using Euler;
using NUnit.Framework;

namespace EulerTests {
	[TestFixture]
	public class Problem2Tests {
		private IProblem2 _classUnderTest;

		[SetUp]
		public void Setup() => _classUnderTest = new Problem2();

		[TestCase(10u, 10u)]
		[TestCase(89u, 44u)]
		[TestCase(4_000_000u, 4_613_732u)]
		public void ShouldReturn_SumOfEvenFibNumbers_UnderN(uint input, uint expected) {
			// Act - When
			var actual = _classUnderTest.SumOfEvenFibonacciNumbers(input);

			// Assert - Then
			Assert.AreEqual(expected, actual);
		}
	}
}
