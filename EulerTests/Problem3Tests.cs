using Euler;
using NUnit.Framework;

namespace EulerTests {
	[TestFixture]
	public class Problem3Tests {
	private IProblem3 _classUnderTest;

		[SetUp]
		public void Setup() => _classUnderTest = new Problem3();

		[TestCase(5u,true)]
		[TestCase(7u,true)]
		[TestCase(13u,true)]
		[TestCase(12u,false)]
		public void ShouldIndicateIfNumberIsPrime(ulong input, bool expected) {
			// Act - When
			var actual = _classUnderTest.IsPrime(input);

			// Assert - Then
			Assert.AreEqual(expected, actual);
		}

		[TestCase(6u, 3u)]
		[TestCase(13195u,29u)]
		[TestCase(600851475143u, 6857u)]
		public void ShouldReturnLargestPrimeFactor(ulong input, ulong expected) {
			// Act - When
			var actual = _classUnderTest.LargestPrimeFactorOfN(input);

			// Assert - Then
			Assert.AreEqual(expected, actual);
		}
	}
}
