﻿using Euler;
using NUnit.Framework;

namespace EulerTests
{
    [TestFixture]
    public class Problem1Tests
    {
        private IProblem1 _classUnderTest;

        [SetUp]
        public void Setup() => _classUnderTest = new Problem1();

        [TestCase(10, 23)]
        [TestCase(1000, 233168)]
        public void ShouldReturnSumOfMultiplesOfN(int input, int expected)
        {
            //Arrange - Given

            //Act - When
            var actual = _classUnderTest.SumOfMultiplesOfN(input);

            //Assert - Then
            Assert.AreEqual(expected, actual);
        }
    }
}
